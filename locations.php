<?php
/**
*
* Template Name: Locations
*
**/

get_header();


echo do_shortcode('[vc_row unlock_row_content="yes" row_height_percent="40" back_color="color-184630" overlay_alpha="50" gutter_size="3" shift_y="0"][vc_column width="1/1"][uncode_slider][vc_row_inner row_inner_height_percent="0" back_image="92" overlay_color="color-jevc" overlay_alpha="50" gutter_size="3" shift_y="0"][vc_column_inner column_width_percent="100" position_vertical="middle" align_horizontal="align_center" gutter_size="3" overlay_alpha="50" medium_width="0" shift_x="0" shift_y="0" z_index="0" width="1/1"][vc_column_text]
<h1 style="text-align: center;"><span style="color: #ffffff;">Locations</span></h1>
[/vc_column_text][/vc_column_inner][/vc_row_inner][/uncode_slider][/vc_column][/vc_row][vc_row back_color="color-xsdn"][vc_column column_width_percent="100" position_vertical="middle" align_horizontal="align_center" overlay_alpha="50" gutter_size="2" medium_width="0" shift_x="0" shift_y="0" z_index="0" width="1/1"][vc_icon display="inline" icon="fa fa-location2" icon_color="color-jevc" size="fa-4x" text_font="font-593073" text_weight="600" text_height=""][/vc_icon][/vc_column][/vc_row]');

?>
<style>
.menu-container{
  position:fixed;
  background: #f7f7f7 !important;
}
  </style>
  <script>
    jQuery("header").removeClass("menu-transparent");
  </script>
<div id="content" class="site-content site-content-locations">
    <div class="location-main">
      <h3>Location Search</h3>
    </div>
    <form method="post" action="" id='search_zip'>
        <input type="text" name="search" id='search' placeholder="Enter city, state or 5-digit zip code">
        <input type='submit' value="search" name="sub" id="search-button" class="btn-color-184630 order-button">
    </form>
</div><!-- .site -->
<div class="content-wrapper">
<div id="map"  class="map-canvas" ></div>

<?php
global $wpdb;
$api_key = 'AIzaSyADO0HuEYwWJkv_-VvKNP6vkZIy-iIXcd8';

if (isset($_POST['sub'])) {
    $search = str_replace(" ", "", $_POST['search']);
    $miles = 30;
    if(strlen($search) == '2'){
        $miles = 75;
    }
    $location = $wpdb->get_row("SELECT google_latitude,google_longitude  FROM wp_ww_api where store_zip='$search' OR store_city='$search' OR store_state='$search'");
    if($location){
        $latitude      = $location->google_latitude;
        $longitude     = $location->google_longitude;
    }
    else{
        $url        = "https://maps.googleapis.com/maps/api/geocode/json?address=".$search."&key=$api_key";
        $details    = file_get_contents($url);
        $result     = json_decode($details,true);
        $latitude   = $result['results'][0]['geometry']['location']['lat'];
        $longitude  = $result['results'][0]['geometry']['location']['lng'];
    }

        $result = $wpdb->get_results("SELECT *,( 3959 * acos( cos( radians($latitude) ) * cos( radians(google_latitude ) ) * cos( radians( google_longitude )
    - radians($longitude) ) + sin( radians($latitude) ) * sin( radians(google_latitude ) ) ) ) AS distance
    FROM wp_ww_api HAVING distance <= $miles ORDER BY distance ASC");
    }
$locations = json_encode($result);

if (count($result) > 0) {
    ?>
   <div class="right-container">
     <div class="location-wrap">
       <h3 class="right-container-plist"><?php echo count($result); ?> Locations Found</h3>
     </div>
       <?php
       foreach ($result as  $res) { ?>
         <div class="single-result">
          <div><h3><?php echo $res->store_city; ?></h3></div>
           <div><p><?php echo $res->address; ?></p></div>
           <div><p><?php echo $res->phone; ?></p></div>
           <div ><p><?php echo $res->fax; ?> (fax)</p></div>
           <p><a href='mailto:<?php echo $res->store_email; ?>'><?php echo $res->store_email; ?> </a></p>

           <div class="hours"><h5>Hours</h5>
           <p><?php echo $res->store_hours_one; ?></p>
           <p><?php echo $res->store_hours_two; ?></p>
           <p><?php echo $res->store_hours_three; ?></p>
           </div>

           <div ><h5>Catering</h5><?php echo $res->catering_phone_number; ?> <br />
             <a href="<?php echo $res->catering_menu_url; ?>" target='_blank'>DOWNLOAD CATERING MENU </a><br/>
           </div>

         <div class="links">
           <span class="btn-container"><a class ="btn-color-184630 order-button" href="<?php echo $res->to_go_menu_url; ?>" target='_blank'>SEE MENU </a></span>
           <span class="btn-container"><a class = "btn-color-184630 order-button" href="<?php echo $res->online_ordering_url; ?>" target='_blank'>ORDER ONLINE </a></span>
         </div>
         </div>
         <hr>
  <?php } ?>
   </div>
 </div>
<script type="text/javascript">
    value = <?php echo $locations; ?>;
    jQuery(document).ready(function () {
     initialize(value);
    });
</script>
<?php }
$api_key = 'AIzaSyADO0HuEYwWJkv_-VvKNP6vkZIy-iIXcd8';
?>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $api_key; ?>"></script>

 <?php
echo do_shortcode('
[vc_row unlock_row_content="yes" row_height_percent="0" back_color="color-184630" overlay_alpha="50" gutter_size="3" shift_y="0"][vc_column column_width_percent="100" position_vertical="middle" align_horizontal="align_center" overlay_alpha="50" gutter_size="3" medium_width="0" shift_x="0" shift_y="0" z_index="0" width="1/1"][vc_column_text]
<h3 style="text-align: center;">Looking for our international locations?</h3>
[/vc_column_text][vc_button button_color="color-jevc" link="url:%2Flocations%2Finternational|||"]View International Locations[/vc_button][/vc_column][/vc_row]

[vc_row][vc_column width="1/1"][uncode_block id="108"][/vc_column][/vc_row]');
 get_footer();?>
