 jQuery("#search_zip").submit(function(){
     value = jQuery('#search').val();
     if(value == '' ){
      alert("Oops! You didn't enter a location.");
      return false;
         }

});
function initialize(value) {
     var total_locations  = value.length;
     var avg_locations    = Math.floor(total_locations/2);
     var center_lat       = value[avg_locations]['google_latitude'];
     var center_long      = value[avg_locations]['google_longitude'];
        var myOptions = {
            center: new google.maps.LatLng(center_lat, center_long),
            zoom: 10,
            scrollwheel: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP

        };
        var map = new google.maps.Map(document.getElementById("map"), myOptions);

        setMarkers(map, value);

    }
   function setMarkers(map, value) {
        var marker, i;


        for (i = 0; i < value.length; i++) {

            var lat                   = value[i]['google_latitude'];
            var long                  =  value[i]['google_longitude'];
            var add                   = value[i]['address'];
            var store_city            = value[i]['store_city'];
            var phone                 = value[i]['phone'];
            var fax                   = value[i]['fax'];
            var store_email           = value[i]['store_email'];
            var catering_phone_number = value[i]['catering_phone_number'];
            var store_hours_one       = value[i]['store_hours_one'];
            var store_hours_two       = value[i]['store_hours_two'];
            var store_hours_three     = value[i]['store_hours_three'];
            var to_go_menu_url        = value[i]['to_go_menu_url'];
            var catering_menu_url     = value[i]['catering_menu_url'];
            var online_ordering_url   = value[i]['online_ordering_url'];

            latlngset = new google.maps.LatLng(lat, long);
             marker = new google.maps.Marker({

          position: latlngset,
          map: map,
          title: store_city,
          draggable:false,
          });

           var city        = "<div class ='location_city'><b>"+store_city+"</b></div>";
           var address     = "<div class='location-title'>" + add +"</div><br/>";
           var phone       = "<div class='location-phone'>" + phone +"</div>";
           var fax         = "<div class='location-fax'>" + fax +" (fax)</div><br/>";
           var store_email = "<a href='mailto:"+ store_email +"'>" + store_email +" </a><br/><br/>";
           var catering_phone_number = "<div class='location-cat'><b>Catering</b>"  + catering_phone_number +" </div><br/>";
           var store_hours       = "<div class='location-hours'><b>Hours</b> \n\
                                  <p>"+store_hours_one+"</p><p>"+store_hours_two+"</p><p>"+store_hours_three+"</p>  </div>";
           var see_menu = "<a href="+to_go_menu_url+" target='_blank'>SEE MENU </a><br/>";
           var catering_menu_url = "<a href="+catering_menu_url+" target='_blank'>DOWNLOAD CATERING MENU </a><br/><br/><br/>";
           var online_ordering_url = "<a href="+online_ordering_url+" target='_blank'>ORDER ONLINE </a><br/><br/><br/>";



            content = city.concat(address,phone,fax,store_email,catering_phone_number,store_hours,see_menu,catering_menu_url,online_ordering_url);

            var infowindow = new google.maps.InfoWindow();

            google.maps.event.addListener(marker, 'click', (function (marker, content) {
                return function () {
                    infowindow.setContent(content);
                    infowindow.open(map, marker);
                };
            })(marker, content, infowindow));

            //Event for mobile devices
            google.maps.event.addListener(marker, 'click', (function (marker, content) {
                return function () {
                    infowindow.setContent(content);
                    infowindow.open(map, marker);
                };
            })(marker, content, infowindow));

        }
    }

//loactions script end
